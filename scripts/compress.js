const child_process = require("child_process")

const compressPNG = file => {
  try {
    child_process.execSync(`pngquant ${file} -f --speed 1 --output ${file}`);
  } catch (error) {
    console.log("No `pngquant` installed, skipping minification.");
  }
};

const largeMaskPath = "static/source/images/js/masks/danger-logo-mask-hero@2x.png";
const heroImagePath = "static/source/images/js/danger-logo-hero@2x.png";
const smallLogoPath = "static/source/images/js/danger-logo-small@2x.png";
const jsRenderedPath = "static/source/images/home/js-logo@2x.png";

[largeMaskPath, smallLogoPath, jsRenderedPath, heroImagePath].forEach(function(element) {
    compressPNG(element)
}, this);
